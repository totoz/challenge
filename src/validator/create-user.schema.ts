export const createUserSchema = {
  tags: ["Users"],
  body: {
    type: "object",
    properties: {
      username: {
        description: "Username",
        type: "string",
      },
      password: {
        description: "Password of user",
        type: "string",
      },
    },
    required: ["username", "password"],
  },
};
