export const refreshTokenSchema = {
  tags: ["Users"],
  body: {
    type: "object",
    properties: {
      refreshToken: {
        description: "refreshToken",
        type: "string",
      },
    },
    required: ["refreshToken"],
  },
};
