import "./config/di";

import { DataContextImpl } from "./factory/datacontext";
import { Config } from "./config";
import { container } from "tsyringe";
import { DataContext, ServerFactory } from "./config/di/services";
import { ServerFactoryImpl } from "./factory/server-factory";

async function main(): Promise<void> {
  const serverFactory = container.resolve<ServerFactory>(ServerFactoryImpl);
  const dataContext = container.resolve<DataContext>(DataContextImpl);
  const server = await serverFactory.build();
  const promiseResult = await Promise.all([
    dataContext.connect(),
    server.listen(Config.fastify.port, Config.fastify.ip),
  ]);
  const address = promiseResult[1];
  console.log(`Server running at: ${address}`);
}

main().catch((err) => {
  console.error(err);
  process.exit(1);
});
