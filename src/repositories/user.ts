import { singleton } from "tsyringe";
import { UserRepository } from "../config/di/repositories";
import { CreateUser } from "../config/types";
import { User } from "../model/user";

@singleton()
export class UserRepositoryImpl implements UserRepository {
  setRefreshToken(refreshToken: string, userId: number): Promise<User[]> {
    return User.query().findById(userId).patch({ refreshToken: refreshToken }).returning("*");
  }

  createAndFetch(createUserDto: CreateUser): Promise<User> {
    return User.query().insert(createUserDto);
  }

  findByUsername(username: string): Promise<User> {
    return User.query().findOne({ username: username.toLowerCase() });
  }
}
