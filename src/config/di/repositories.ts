/**
 * Repositories
 */

import { container } from "tsyringe";
import { User } from "../../model/user";
import { UserRepositoryImpl } from "../../repositories/user";
import { CreateUser } from "../types";

export interface UserRepository {
  setRefreshToken(refreshToken: string, userId: number): Promise<User[]>;
  createAndFetch(createUserDto: CreateUser): Promise<User>;
  findByUsername(username: string): Promise<User>;
}

container.register<UserRepository>("UserRepository", { useClass: UserRepositoryImpl });
