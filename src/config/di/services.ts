/**
 * Services
 */

import { Config } from "knex";
import { FastifyInstance } from "fastify/types/instance";
import { ServerOptions } from "http";
import { container } from "tsyringe";
import { DataContextImpl } from "../../factory/datacontext";
import { AuthServiceImpl } from "../../services/auth";
import { CreateUser, Credentials, LoginCredentials, RefreshToken } from "../types";

export interface ServerFactory {
  build(options?: ServerOptions): Promise<FastifyInstance>;
  getInstance(): Promise<FastifyInstance>;
  stopServer(): Promise<void>;
}

export interface DataContext {
  close(): Promise<void>;
  isConnected(): Promise<boolean>;
  connect(cfg?: Config): Promise<void>;
}

export interface AuthService {
  refreshToken(body: RefreshToken): Credentials;
  registerUser(registerUser: CreateUser): Promise<void>;
  loginUser(loginData: LoginCredentials): Promise<Credentials>;
}

container.register<AuthService>("AuthService", { useClass: AuthServiceImpl });
container.register<DataContext>("DataContext", { useClass: DataContextImpl });
