export type Credentials = {
  accessToken: string;
  refreshToken: string;
};
export type LoginCredentials = {
  username: string;
  password: string;
};
export type CreateUser = {
  username: string;
  password: string;
};

export type RefreshToken = {
  refreshToken: string;
};
