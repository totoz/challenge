import { config } from "dotenv";
import { Config as KnexConfig } from "knex";
import { knexSnakeCaseMappers } from "objection";
config();

export const Config = {
  database: {
    debug: false,
    client: "pg",
    connection: {
      host: process.env.DB_HOST ? process.env.DB_HOST : "127.0.0.1",
      user: process.env.DB_USER ? process.env.DB_USER : "postgres",
      port: process.env.DB_PORT ? Number(process.env.DB_PORT) : 5432,
      password: process.env.DB_PASSWORD ? process.env.DB_PASSWORD : "docker",
      database: process.env.DB_NAME ? process.env.DB_NAME : "challenge",
      charset: "utf8",
    } as KnexConfig,
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
    ...knexSnakeCaseMappers(),
  },
  fastify: {
    ip: process.env.FASTIFY_IP ? process.env.FASTIFY_IP : "127.0.0.1",
    port: process.env.FASTIFY_PORT ? Number(process.env.FASTIFY_PORT) : 3000,
  },
  jwt: {
    secret: process.env.JWT_SECRET ? process.env.JWT_SECRET : "changeit",
  },
};
