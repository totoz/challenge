import { BaseModel } from "./base-model";
import { compare, hash } from "bcryptjs";
import { ModelOptions, Pojo, QueryContext } from "objection";

export class User extends BaseModel {
  username!: string;
  password!: string;
  refreshToken?: string;

  static tableName = "user";

  $formatJson(json: Pojo): Pojo {
    json = super.$formatJson(json);
    delete json["password"];
    return json;
  }

  verifyPassword(password: string): Promise<boolean> {
    return compare(password, this.password);
  }

  $beforeInsert(queryContext: QueryContext): Promise<void> {
    super.$beforeInsert(queryContext);
    return this.generateHash();
  }

  public $beforeUpdate(opt: ModelOptions, queryContext: QueryContext): void | Promise<unknown> {
    super.$beforeUpdate(opt, queryContext);
    if (opt.patch && this.password === undefined) {
      return;
    }
    return this.generateHash();
  }

  private isBcryptHash(str: string): boolean {
    const BCRYPT_REGEXP = /^\$2[ayb]\$.{56}$/;
    return BCRYPT_REGEXP.test(str);
  }

  private async generateHash(): Promise<void> {
    if (this.password && this.password.length > 0) {
      if (this.isBcryptHash(this.password)) {
        throw new Error("Bcrypt tried to hash another Bcrypt hash");
      }
      const passwordHash = await hash(this.password, 0);
      this.password = passwordHash;
    }
    throw new Error("password must not be empty");
  }
}
