import { Model, ModelOptions, QueryContext } from "objection";

export class BaseModel extends Model {
  readonly id!: number;
  deletedAt: Date;
  updatedAt: Date;
  createdAt: Date;

  $beforeInsert(context: QueryContext): void {
    super.$beforeInsert(context);
    this.createdAt = new Date();
    this.updatedAt = new Date();
  }

  $beforeUpdate(opt: ModelOptions, context: QueryContext): void {
    super.$beforeUpdate(opt, context);
    this.updatedAt = new Date();
  }
}
