export class ChallengeException extends Error {
  constructor(message = "") {
    super();
    this.message = message;
  }
}
