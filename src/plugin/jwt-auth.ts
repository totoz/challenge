import fp from "fastify-plugin";

import { FastifyInstance, FastifyPluginAsync, FastifyReply, FastifyRequest } from "fastify";
import { verify } from "jsonwebtoken";
import { RouteGenericInterface } from "fastify/types/route";
import { Server, IncomingMessage, ServerResponse } from "http";
import { Config } from "../config";

type JwtData = {
  username: string;
  id: string;
};

declare module "fastify" {
  interface FastifyRequest {
    jwtData: JwtData;
  }
}

function isNotPublicPath(url: string) {
  return !url.startsWith("/documentation") && !url.startsWith("/api/auth");
}

function verifyToken(
  request: FastifyRequest<RouteGenericInterface, Server, IncomingMessage>,
  reply: FastifyReply<Server, IncomingMessage, ServerResponse, RouteGenericInterface, unknown>,
) {
  try {
    const bearerToken = request.headers.authorization;
    const splitedBearer = bearerToken.split(" ");
    const token = splitedBearer[1];
    request.jwtData = verify(token, Config.jwt.secret) as JwtData;
  } catch (err) {
    console.log(err, "<<");
    reply.unauthorized();
  }
}

const plugin: FastifyPluginAsync = async (fastify: FastifyInstance) => {
  fastify.addHook("onRequest", async (request, reply) => {
    if (isNotPublicPath(request.url)) {
      verifyToken(request, reply);
    } else {
      reply.unauthorized();
    }
  });
};

export default fp(plugin, { name: "auth-plugin" });
