import fp from "fastify-plugin";
import swagger from "fastify-swagger";
import middie from "middie";
import cors from "fastify-cors";
import sensible from "fastify-sensible";

import { FastifyInstance, FastifyPluginAsync } from "fastify";
import { UnauthorizedException } from "../exception/unauthorized.exception";

const corsOpts = {
  origin: "*",
  credentials: true,
  methods: ["GET", "PUT", "PATCH", "POST", "DELETE"],
};

const swaggerOpts = {
  routePrefix: "/documentation",
  exposeRoute: true,
  swagger: {
    info: {
      title: "Projects API",
      version: "1.0.0",
    },
    schemes: ["http"],
    consumes: ["application/json"],
    produces: ["application/json"],
  },
};

function isValidationError(error): boolean {
  return Array.isArray(error.validation) && error.validation.length > 0;
}

const plugin: FastifyPluginAsync = async (fastify: FastifyInstance) => {
  fastify
    .register(cors, corsOpts)
    .register(swagger, swaggerOpts)
    .register(middie)
    .register(sensible)
    .after(() =>
      fastify.setErrorHandler((error, _request, reply) => {
        switch (error.constructor) {
          case UnauthorizedException:
            reply.unauthorized();
            break;
          default:
            if (isValidationError(error)) {
              reply.badRequest(error.message);
            }
            reply.internalServerError();
            break;
        }
      }),
    );
};

export default fp(plugin, { name: "generic-plugin" });
