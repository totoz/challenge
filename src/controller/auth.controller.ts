import { FastifyInstance } from "fastify";
import { container } from "tsyringe";
import { loginSchema } from "../validator/login.schema";
import { createUserSchema } from "../validator/create-user.schema";
import { refreshTokenSchema } from "../validator/refresh-token.schema";
import { AuthService } from "../config/di/services";
import { CreateUser, LoginCredentials, RefreshToken } from "../config/types";
import { AuthServiceImpl } from "../services/auth";

export default async function (server: FastifyInstance): Promise<void> {
  const authService = container.resolve<AuthService>(AuthServiceImpl);
  server.post<{ Body: CreateUser }>(
    "/",
    {
      schema: loginSchema,
    },
    async (request, reply) => {
      await authService.registerUser(request.body);
      reply.status(204);
      return reply;
    },
  );

  server.post<{ Body: LoginCredentials }>(
    "/login",
    {
      schema: createUserSchema,
    },
    async (request, _reply) => {
      return authService.loginUser(request.body);
    },
  );

  server.post<{ Body: RefreshToken }>(
    "/refreshToken",
    {
      schema: refreshTokenSchema,
    },
    async (request, _reply) => {
      return authService.refreshToken(request.body);
    },
  );
}

module.exports.autoPrefix = "/api/users";
