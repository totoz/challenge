import { Config } from "../config";
import Knex from "knex";
import { BaseModel } from "../model/base-model";
import { Transaction, transaction } from "objection";
import { singleton } from "tsyringe";
import { DataContext } from "../config/di/services";

@singleton()
export class DataContextImpl implements DataContext {
  public startTransaction(): Promise<Transaction> {
    return transaction.start(BaseModel.knex());
  }

  async close(): Promise<void> {
    try {
      await BaseModel.knex().destroy();
    } catch (error) {
      throw new Error(`${JSON.stringify(error, null, 2)}`);
    }
  }

  async isConnected(): Promise<boolean> {
    try {
      await BaseModel.knex().raw("select 1+1 as result");
      return true;
    } catch (error) {
      return false;
    }
  }

  async connect(cfg?: Knex.Config): Promise<void> {
    if (!(await this.isConnected())) {
      if (cfg === undefined) {
        BaseModel.knex(Knex(Config.database));
      } else {
        BaseModel.knex(Knex(cfg));
      }
      await BaseModel.knex().raw("select 1+1 as result");
    }
  }
}
