import { inject, singleton } from "tsyringe";
import { AuthService } from "../config/di/services";
import { CreateUser, Credentials, LoginCredentials, RefreshToken } from "../config/types";
import { UnauthorizedException } from "../exception/unauthorized.exception";
import { sign, verify } from "jsonwebtoken";
import { Config } from "../config";
import { UserRepository } from "../config/di/repositories";
import { ChallengeException } from "../exception/challenge.exception";

@singleton()
export class AuthServiceImpl implements AuthService {
  constructor(@inject("UserRepository") private readonly userRepo: UserRepository) {}

  public async loginUser(body: LoginCredentials): Promise<Credentials> {
    const user = await this.findUserAndVerifyPassword(body);
    if (!this.refreshTokenIsValid(user.refreshToken)) {
      user.refreshToken = this.createRefreshToken(user.username);
      await this.userRepo.setRefreshToken(user.refreshToken, user.id);
    }
    const accessToken = sign({ foo: "bar" }, Config.jwt.secret);
    return { accessToken: accessToken, refreshToken: user.refreshToken };
  }

  public refreshToken(body: RefreshToken): Credentials {
    try {
      verify(body.refreshToken, Config.jwt.secret);
    } catch (error) {
      throw new UnauthorizedException();
    }
    const accessToken = sign({ foo: "bar" }, Config.jwt.secret);
    return { accessToken: accessToken, refreshToken: body.refreshToken };
  }

  public async registerUser(registerUser: CreateUser): Promise<void> {
    const userExists = await this.userRepo.findByUsername(registerUser.username);
    if (userExists) {
      throw new ChallengeException("Username already taken");
    }
    await this.userRepo.createAndFetch(registerUser);
  }

  private async findUserAndVerifyPassword(loginDto: LoginCredentials) {
    const user = await this.userRepo.findByUsername(loginDto.username);
    if (user && (await user.verifyPassword(loginDto.password))) {
      return user;
    }
    throw new UnauthorizedException();
  }

  private refreshTokenIsValid(refreshToken: string) {
    try {
      verify(refreshToken, Config.jwt.secret);
      return true;
    } catch (error) {
      return false;
    }
  }

  private createRefreshToken(username: string) {
    return sign({}, Config.jwt.secret, { subject: username, expiresIn: "7d" });
  }
}
