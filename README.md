# Challenge

[https://github.com/improvein/dev-challenge-nodejs](https://github.com/improvein/dev-challenge-nodejs)

For simplicity no test were added. But DI was used to enable easy unit testing.

## Used Technologies and Frameworks

- Fastify (web framework)
- Objectionjs (ORM like )/ Knexjs (query builder and migration manager)
- tsyringe (dependency injection)
- TypeScript
- jwt
- prettier
- PostgreSQL
- yarn

## How to run

> By default the app uses some values to configure the service. You can change this values, just take a look at the end of this file to see which values you can customize.

- Install deps. (`yarn`)
- Make sure to have a Postgres server running with a database named as you like (default: challenge).
- Run the database migration: `yarn knex migrate:latest`
- Populate the default values: `yarn knex seed:run`
- Run the app: `yarn start:dev` or `yarn build; yarn start:prod`

And endpoit will expose the swagger documentation, by default: `127.0.0.1:3000/documentation`

## Default properties

This values can be added to a `.env` file at the root of the project or can be pass as env variables.

```bash
DB_HOST=127.0.0.1
DB_PORT=5432
DB_USER=postgres
DB_PASSWORD=docker
DB_NAME=challenge
FASTIFY_IP=127.0.0.1
FASTIFY_PORT=3000
JWT_SECRET=changeit
```

### ERM

![](https://plantuml-server.kkeisuke.app/svg/jLNVRzem47xFNt5B7qea2ftsXgAeYD5jqjPA2EqLEV51B1mxivrIDEr_tzb9A6gfXBJ1IoxttO_lktFCdNNCkAQIqJKqgj1LXSg1An6aK0WvIhq3iOQzRguDGcE5sW25rOr30plXIc1Wq0d5W0jRIxQdIdOhLCqCgu3N0ZxTtaOVEAvzmINB9GuKgt38qHO9599P2zu1p4BidJ6aAGoMmwlx-odz4iWoghdptOcHbXpMsa1jHCNC7hReEsuCeY9YaX2FDahyQf00fZYzS3Isq2J0OqgxbMgaF93gIgserE0vWJJFdjDndXtmjbY0GeT9wEQ3mkjBO5yvX3O-eF7KDStJW6CR2-o92UbdC25_Ah9qB7n4chDYEvazVdzSP7tL8Ume3JjHWd8xCmoBdgMhPESgDFTY1s5airdYTu0ueH1NJyPZMeloInxHviPXucX7Fltg1mqbAWL7gD2LcjlsnT9sKRjzZJQYMdGcX1FURlV7XS72QJEaXNLcdC3lw7YGWYVmxM4v_p9VH8UvUEe9_9mkPb-dHswBpoUykJ2kN77cAENpT3cF05x0mY01VCLS1nwGfkO1EKhaAB5tjPu_lPHAFmaa7U5vdeYOXY5zx1l6XlemYrth7hu4xPY5eHDnTlxqOvfpV65Uz8q6Mk5v7YiVwW_6gOd6DHtMdJRy23IuDcZBbTDR-kOlgiOzhMofTrvGRvwdwJqzbiw7LlPK2XCcS8WlSjfxHIq1BwYppjJJL7bOw1i8Qs7fzZafoT_cHjHEX3uNtLH7mmlhpN_RLAlu9UC_hoFgpXF4jwFHGmmtDnd47sFexzCeN4ZlektoNiDTpPErMvEzK_iOxURtkdZRafZVeUBqP_8N.svg)
