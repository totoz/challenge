import Knex from "knex";

export const up = async function (knex: Knex): Promise<void> {
  await knex.schema.createTable("user", function (tableBuilder) {
    tableBuilder.increments("id").primary();
    tableBuilder.string("username").notNullable();
    tableBuilder.string("password").notNullable();
    tableBuilder.string("refresh_token").nullable();
    tableBuilder.dateTime("created_at").notNullable().defaultTo(knex.fn.now());
    tableBuilder.dateTime("updated_at").nullable().defaultTo(knex.fn.now());
    tableBuilder.dateTime("deleted_at").nullable();
  });
};

export const down = async function (knex: Knex): Promise<void> {
  await knex.schema.dropTable("user");
};
