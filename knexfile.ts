import { Config } from "./src/config";
module.exports = {
  development: {
    ...Config.database,
    migrations: {
      tableName: "knex_migrations",
    },
  },
};
